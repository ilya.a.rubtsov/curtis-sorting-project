# curtis-sorting-project

## How to run the project

1. Build the project with gradle "jar" task;
2. Run ```java -jar -Xmx512m curtis-sorting-project-1.0-SNAPSHOT.jar``` and add one line of available arguments:
   1. ```generate <long numberOfLines> <int maxLineLength>```
   2. ```sort <String inputFilePath>```
   3. ```sort <String inputFilePath> <String outputFilePath>```
   4. ```generate-sort <long numberOfLines> <int maxLineLength>```
   5. ```generate-sort <long numberOfLines> <int maxLineLength> <String outputFilePath>```

- ```numberOfLines``` - number of lines to be randomly generated
- ```maxLineLength``` - max number of characters in each line
- ```inputFilePath``` - path and name of file to sort
- ```outputFilePath``` - path and name of a sorted file
