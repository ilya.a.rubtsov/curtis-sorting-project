package ru.iar.sorting;

import ru.iar.sorting.service.filegenerator.FileGeneratorService;
import ru.iar.sorting.service.filegenerator.FileGeneratorServiceImpl;
import ru.iar.sorting.service.externalsorting.ExternalSortService;
import ru.iar.sorting.service.externalsorting.ExternalSortServiceImpl;

import java.io.File;
import java.util.Arrays;

public class FileSorter {
    public static void main(String[] args) {
        FileGeneratorService fileGeneratorService = new FileGeneratorServiceImpl();
        ExternalSortService externalSortService = new ExternalSortServiceImpl();
        String help = "Add one line of available arguments:"
                + "\n" + "1. generate <long numberOfLines> <int maxLineLength>"
                + "\n" + "2. sort <String inputFilePath>"
                + "\n" + "3. sort <String inputFilePath> <String outputFilePath>"
                + "\n" + "4. generate-sort <long numberOfLines> <int maxLineLength>"
                + "\n" + "5. generate-sort <long numberOfLines> <int maxLineLength> <String outputFilePath>"
                ;
        if(Arrays.stream(args).findFirst().isEmpty()) {
            System.out.println(help);
            return;
        }
        switch(args[0]) {
            case "generate":
                fileGeneratorService.generateFile(Long.parseLong(args[1]), Integer.parseInt(args[2]));
                break;
            case "sort":
                if(args.length == 2) {
                    externalSortService.sort(new File(args[1]));
                } else {
                    externalSortService.sort(new File(args[1]), new File(args[2]));
                }
                break;
            case "generate-sort":
                File generatedFile = fileGeneratorService
                        .generateFile(Long.parseLong(args[1]), Integer.parseInt(args[2]));
                if(args.length == 3) {
                    externalSortService.sort(generatedFile);
                } else  {
                    externalSortService.sort(generatedFile, new File(args[3]));
                }
                break;
            default:
                System.out.println(help);
        }
    }
}
