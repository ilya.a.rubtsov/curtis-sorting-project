package ru.iar.sorting.service.externalsorting;

import java.io.File;

public interface ExternalSortService {
    File sort(File inputFile);
    File sort(File inputFile, File outputFile);
}
