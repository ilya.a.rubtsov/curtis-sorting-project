package ru.iar.sorting.service.externalsorting;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

public final class ExternalSortServiceImpl implements ExternalSortService {
    private static final int DEFAULT_MAX_TEMP_FILES = 1024;
    private static final Comparator<String> DEFAULT_COMPARATOR = String::compareTo;
    private static final String DEFAULT_SORTED_FILE_PATH = "sorted-file.txt";

    public File sort(File inputFile) {
        return sort(inputFile, new File(DEFAULT_SORTED_FILE_PATH));
    }

    @Override
    public File sort(File inputFile, File outputFile) {
        if(inputFile == null) {
            throw new IllegalArgumentException("Input file must not be null in sort method of " + this.getClass());
        }
        if(outputFile == null) {
            outputFile = new File(DEFAULT_SORTED_FILE_PATH);
        }
        try {
            System.out.println("Sorting file: " + inputFile);
            mergeSortedFiles(sortInBatch(inputFile), outputFile);
            System.out.println("Sorting finished. Output file: " + outputFile);
        } catch (IOException e) {
            throw new RuntimeException("Invalid input/output in sort method of " + this.getClass());
        }
        return outputFile;
    }

    private List<File> sortInBatch(File file) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        long dataLength = file.length();
        long maxMemory = estimateAvailableMemory();
        return sortInBatch(bufferedReader, dataLength, maxMemory);
    }

    private List<File> sortInBatch(BufferedReader bufferedReader, long dataLength,
                                   long maxMemory) throws IOException {
        List<File> files = new ArrayList<>();
        long blockSize = estimateBestSizeOfBlocks(dataLength, maxMemory);
        try (bufferedReader) {
            List<String> tmpList = new ArrayList<>();
            String line = "";
            try {
                while (line != null) {
                    long currentBlockSize = 0;
                    while ((currentBlockSize < blockSize)
                            && ((line = bufferedReader.readLine()) != null)) {
                        tmpList.add(line);
                        currentBlockSize += StringSizeEstimator.estimatedSizeOf(line);
                    }
                    files.add(sortAndSave(tmpList));
                    tmpList.clear();
                }
            } catch (EOFException e) {
                if (tmpList.size() > 0) {
                    files.add(sortAndSave(tmpList));
                    tmpList.clear();
                }
            }
        }
        return files;
    }

    private void mergeSortedFiles(BufferedWriter bufferedWriter,
                                  List<BinaryFileBuffer> binaryFileBuffers) throws IOException {
        PriorityQueue<BinaryFileBuffer> priorityQueue =
                new PriorityQueue<>((i, j) -> DEFAULT_COMPARATOR.compare(i.peek(), j.peek()));
        for (BinaryFileBuffer binaryFileBuffer : binaryFileBuffers) {
            if (!binaryFileBuffer.empty()) {
                priorityQueue.add(binaryFileBuffer);
            }
        }
        try (bufferedWriter) {
            while (priorityQueue.size() > 0) {
                BinaryFileBuffer binaryFileBuffer = priorityQueue.poll();
                String r = binaryFileBuffer.pop();
                bufferedWriter.write(r);
                bufferedWriter.newLine();
                if (binaryFileBuffer.empty()) {
                    binaryFileBuffer.close();
                } else {
                    priorityQueue.add(binaryFileBuffer);
                }
            }
        } finally {
            for (BinaryFileBuffer binaryFileBuffer : priorityQueue) {
                binaryFileBuffer.close();
            }
        }
    }

    private void mergeSortedFiles(List<File> files, File outputFile) throws IOException {
        ArrayList<BinaryFileBuffer> binaryFileBuffers = new ArrayList<>();
        for (File file : files) {
            InputStream inputStream = new FileInputStream(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            BinaryFileBuffer binaryFileBuffer = new BinaryFileBuffer(bufferedReader);
            binaryFileBuffers.add(binaryFileBuffer);
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));
        mergeSortedFiles(bufferedWriter, binaryFileBuffers);
        for (File file : files) {
            file.delete();
        }
    }

    private File sortAndSave(List<String> tmpList) throws IOException {
        tmpList = tmpList.parallelStream()
                .sorted(DEFAULT_COMPARATOR)
                .collect(Collectors.toCollection(ArrayList<String>::new));
        File newTmpFile = File.createTempFile("sortInBatch", "flatFile");
        newTmpFile.deleteOnExit();
        OutputStream outputStream = new FileOutputStream(newTmpFile);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream))) {
            for (String r : tmpList) {
                bufferedWriter.write(r);
                bufferedWriter.newLine();
            }
        }
        return newTmpFile;
    }

    private long estimateAvailableMemory() {
        System.gc();
        Runtime r = Runtime.getRuntime();
        long allocatedMemory = r.totalMemory() - r.freeMemory();
        return r.maxMemory() - allocatedMemory;
    }

    private long estimateBestSizeOfBlocks(final long sizeOfFile, final long maxMemory) {
        long blockSize = sizeOfFile / DEFAULT_MAX_TEMP_FILES + (sizeOfFile % DEFAULT_MAX_TEMP_FILES == 0 ? 0 : 1);
        if (blockSize < maxMemory / 2) {
            blockSize = maxMemory / 2;
        }
        return blockSize;
    }

    /**
     * This is essentially a thin wrapper on top of a BufferedReader... which keeps
     * the last line in memory.
     */
    private final static class BinaryFileBuffer {
        private final BufferedReader bufferedReader;
        private String cache;
        public BinaryFileBuffer(BufferedReader bufferedReader) throws IOException {
            this.bufferedReader = bufferedReader;
            reload();
        }
        public void close() throws IOException {
            this.bufferedReader.close();
        }
        public boolean empty() {
            return this.cache == null;
        }
        public String peek() {
            return this.cache;
        }
        public String pop() throws IOException {
            String answer = peek();
            reload();
            return answer;
        }
        private void reload() throws IOException {
            this.cache = this.bufferedReader.readLine();
        }
    }

    /**
     * Simple class used to estimate memory usage.
     * @author Eleftherios Chetzakis
     */
    private final static class StringSizeEstimator {
        private static final int OBJ_OVERHEAD;

        /**
         * Private constructor to prevent instantiation.
         */
        private StringSizeEstimator() {
        }

        /*
         * Class initializations.
         */
        static {
            // By default, we assume 64 bit JVM
            // (defensive approach since we will get
            // larger estimations in case we are not sure)
            boolean IS_64_BIT_JVM = true;
            // check the system property "sun.arch.data.model"
            // not very safe, as it might not work for all JVM implementations
            // nevertheless the worst thing that might happen is that the JVM is 32bit,
            // but we assume its 64bit, so we will be counting a few extra bytes per string object
            // no harm done here since this is just an approximation.
            String arch = System.getProperty("sun.arch.data.model");
            if (arch != null) {
                if (arch.contains("32")) {
                    // If exists and is 32 bit then we assume a 32bit JVM
                    IS_64_BIT_JVM = false;
                }
            }
            // The sizes below are a bit rough as we don't take into account
            // advanced JVM options such as compressed oops
            // however if our calculation is not accurate it'll be a bit over
            // so there is no danger of an out of memory error because of this.
            int OBJ_HEADER = IS_64_BIT_JVM ? 16 : 8;
            int ARR_HEADER = IS_64_BIT_JVM ? 24 : 12;
            int OBJ_REF = IS_64_BIT_JVM ? 8 : 4;
            int INT_FIELDS = 12;
            OBJ_OVERHEAD = OBJ_HEADER + INT_FIELDS + OBJ_REF + ARR_HEADER;
        }

        /**
         * Estimates the size of a {@link String} object in bytes.
         * This function was designed with the following goals in mind (in order of importance) :
         * First goal is speed: this function is called repeatedly, and it should
         * execute in not much more than a nanosecond.
         * Second goal is to never underestimate (as it would lead to memory shortage and a crash).
         * Third goal is to never overestimate too much (say within a factor of two), as it would
         * mean that we are leaving much of the RAM underutilized.
         *
         * @param s The string to estimate memory footprint.
         * @return The <strong>estimated</strong> size in bytes.
         */
        public static long estimatedSizeOf(String s) {
            return (s.length() * 2L) + OBJ_OVERHEAD;
        }
    }
}
