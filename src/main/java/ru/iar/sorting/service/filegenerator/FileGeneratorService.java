package ru.iar.sorting.service.filegenerator;

import java.io.File;

public interface FileGeneratorService {
    File generateFile(long numberOfLines, int maxLineLength);
    File generateFile(long numberOfLines, int maxLineLength, String outputAbsPath);
}
