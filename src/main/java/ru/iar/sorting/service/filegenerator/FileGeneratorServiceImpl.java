package ru.iar.sorting.service.filegenerator;

import java.io.*;

public final class FileGeneratorServiceImpl implements FileGeneratorService {
    private static final String DEFAULT_OUTPUT_PATH = "generated-file.txt";
    private static final String DEFAULT_SET_OF_CHARACTERS = "0123456789" + "abcdefghijklmnopqrstuvwxyz";

    public File generateFile(long numberOfLines, int maxLineLength) {
        return generateFile(numberOfLines, maxLineLength, DEFAULT_OUTPUT_PATH);
    }

    @Override
    public File generateFile(long numberOfLines, int maxLineLength, String outputAbsPath) {
        if(outputAbsPath == null) {
            outputAbsPath = DEFAULT_OUTPUT_PATH;
        }
        File outputFile = new File(outputAbsPath);
        if(numberOfLines <= 0) {
            throw new IllegalArgumentException("Number of lines must be greater than 0");
        }
        if(maxLineLength <= 0) {
            throw new IllegalArgumentException("Max line length must be greater than 0");
        }
        try(BufferedWriter bufferedWriter =
                    new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)))) {
            System.out.println("Generating file: " + outputFile);
            for(long i = 0; i <= numberOfLines; i++) {
                bufferedWriter.write(generateString(maxLineLength));
                bufferedWriter.newLine();
            }
            System.out.println("Generating finished.");
        } catch (IOException ioe) {
            System.out.println("Invalid input/output in generateFile method of " + this.getClass());
        }
        return outputFile;
    }

    private String generateString(int maxLength) {
        if(maxLength <= 0) {
            throw new IllegalArgumentException("Max length must be greater than 0");
        }
        int leftLimit = 1;
        int generatedLong = leftLimit + (int) (Math.random() * (maxLength - leftLimit));
        StringBuilder stringBuilder = new StringBuilder(generatedLong);
        for (int i = 0; i < generatedLong; i++) {
            int index = (int) (DEFAULT_SET_OF_CHARACTERS.length() * Math.random());
            stringBuilder.append(DEFAULT_SET_OF_CHARACTERS.charAt(index));
        }
        return stringBuilder.toString();
    }
}
