package ru.iar.sorting.service.externalsorting;

import org.junit.jupiter.api.*;

import java.io.*;

class ExternalSortServiceImplTest {
    File unsortedFile;
    File sortedFile;
    File fileSortedByExternalSort;
    final String SORTED_SET_OF_CHARACTERS = "0123456789" + "abcdefghijklmnopqrstuvwxyz";
    final String UNSORTED_SET_OF_CHARACTERS = "zyxwvutsrqponmlkjihgfedcba" + "9876543210";
    final ExternalSortService externalSortService = new ExternalSortServiceImpl();

    @BeforeEach
    void setUp() {
        this.unsortedFile = new File("unsortedFile.txt");
        BufferedWriter bufferedWriterUnsorted = getBufferedWriter(unsortedFile);
        writeCharsToFile(bufferedWriterUnsorted, UNSORTED_SET_OF_CHARACTERS);
        this.sortedFile = new File("sortedFile.txt");
        BufferedWriter bufferedWriterSorted = getBufferedWriter(sortedFile);
        writeCharsToFile(bufferedWriterSorted, SORTED_SET_OF_CHARACTERS);
    }

    @AfterEach
    void tearDown() {
        if(unsortedFile != null) {
            unsortedFile.delete();
        }
        if(sortedFile != null) {
            sortedFile.delete();
        }
        if(fileSortedByExternalSort != null) {
            fileSortedByExternalSort.delete();
        }
    }

    @Test
    void testIfNullInputFileThrowsRuntimeException() {
        //given default conditions
        //when inputFile = null
        //then
        Assertions.assertThrows(RuntimeException.class, () -> externalSortService.sort(null));
    }

    @Test
    void testIfOutputFileIsSorted() {
        //given unsortedFile and
        String expectedResult = fileToString(sortedFile);
        //when
        fileSortedByExternalSort = externalSortService.sort(unsortedFile);
        String actualResult = fileToString(fileSortedByExternalSort);
        //then
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    @Disabled
    void testSortingWithTwoArguments() {
        //TODO
    }

    @Test
    @Disabled
    void testSortingWithNullAsSecondArgument() {
        //TODO
    }

    @Test
    @Disabled
    void testSortingWithBothArgumentsAsNull() {
        //TODO
    }

    private BufferedWriter getBufferedWriter(File file) {
        try {
            return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
        } catch (IOException e) {
            throw new RuntimeException("Couldn't get BufferedWriter", e);
        }
    }

    private void writeCharsToFile(BufferedWriter bufferedWriter, String setOfCharacters) {
        try(bufferedWriter) {
            for(int i = 0; i < setOfCharacters.length(); i++) {
                bufferedWriter.write(setOfCharacters.charAt(i));
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException("Couldn't write to file", e);
        }
    }

    private String fileToString(File file) {
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            StringBuilder stringBuilder = new StringBuilder();
            String sCurrentLine;
            while ((sCurrentLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(sCurrentLine).append("\n");
            }
            return stringBuilder.toString();
        } catch (IOException e) {
            throw new RuntimeException("Couldn't read file to String", e);
        }
    }
}
